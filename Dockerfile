include(bookworm)

ARG PG_VERSION="16"

RUN <<EOF
  apt update
  apt upgrade --yes
  apt install --yes \
      wget gnupg apt-transport-https lsb-release ca-certificates

  ### add postgresql repository
  key_file=/usr/share/keyrings/postgresql.asc
  wget -O $key_file https://www.postgresql.org/media/keys/ACCC4CF8.asc
  repo_url=http://apt.postgresql.org/pub/repos/apt
  apt_config=/etc/apt/sources.list.d/postgresql.list
  echo "deb [signed-by=$key_file] $repo_url $(lsb_release -cs)-pgdg main" > $apt_config
  apt update

  ### install postgresql
  DEBIAN_FRONTEND=noninteractive \
  apt install --yes \
      postgresql-${PG_VERSION} \
      postgresql-contrib \
      postgresql-${PG_VERSION}-postgis-3 \
      postgresql-${PG_VERSION}-postgis-3-scripts

  ### backup directories that will be mounted
  cp -a /var/lib/postgresql /var/lib/postgresql1
  cp -a /etc/postgresql /etc/postgresql1
EOF
