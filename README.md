# PostgreSQL in a Container

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull postgresql`

  - Create a directory for the container: `ds init postgresql @postgresql`

  - Fix the settings: `cd /var/ds/postgresql/ && vim settings.sh`

  - Build image, create the container and configure it: `ds make`
