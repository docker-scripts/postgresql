APP=postgresql

PG_VERSION="16"

### Uncomment PORTS if you want this postgresql container
### to be accessed from the internet.
#PORTS="5432:5432"
