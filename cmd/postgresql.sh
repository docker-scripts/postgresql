cmd_postgresql_help() {
    cat <<_EOF
    postgresql [ create | drop | dump | sql | script ]
        Manage the database of the container '$CONTAINER'.
        - create
              create the user '$DB_USER' and the database '$DB_NAME'
        - drop
              drop the user '$DB_USER' and the database '$DB_NAME'
        - dump
              dump the database '$DB_NAME'
        - sql "<query>"
              run the given sql query on '$DB_NAME'
        - script <file.sql>
              run the given sql script on '$DB_NAME'

_EOF
}

pg() {
    if test -t 0 ; then
        docker exec -it \
               -u postgres -w /var/lib/postgresql \
               $DB_HOST env TERM=xterm "$@"
    else
        docker exec -i \
               -u postgres -w /var/lib/postgresql \
               $DB_HOST "$@"
    fi
}

cmd_postgresql() {
    [[ -n $DB_HOST ]] || fail "Error: No DB_HOST defined on 'settings.sh'"

    local cmd=$1
    shift
    case $cmd in
        sql)
            [[ -n $1 ]] || fail "Usage:\n$(cmd_postgresql_help)"
            pg psql -d $DB_NAME -c "$1"
            ;;
        drop)
            pg dropdb   --if-exists $DB_NAME
            pg dropuser --if-exists $DB_USER
            ;;
        create)
            pg psql -d $DB_NAME -c '\q' &>/dev/null || {
                pg createuser --createdb $DB_USER
                pg psql -c "ALTER USER $DB_USER WITH PASSWORD '$DB_PASS';"
                pg createdb --owner=$DB_USER $DB_NAME
            }
            ;;
        dump)
            pg pg_dump --clean -d $DB_NAME
            ;;
        script)
            [[ -n $1 ]] || fail "Usage:\n$(cmd_postgresql_help)"
            local sqlfile=$1
            docker cp $sqlfile $DB_HOST:/var/lib/postgresql/
            local tmp_sqlfile=/var/lib/postgresql/$(basename $sqlfile)
            docker exec $DB_HOST chown postgres: $tmp_sqlfile
            pg psql -d $DB_NAME -f $tmp_sqlfile -o /dev/null
            docker exec $DB_HOST rm $tmp_sqlfile
            ;;
        *)
            echo -e "Usage:\n$(cmd_postgresql_help)"
            exit
            ;;
    esac
}
