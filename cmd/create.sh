rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p var etc dblogs
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/var,dst=/var/lib/postgresql \
        --mount type=bind,src=$(pwd)/etc,dst=/etc/postgresql \
        --mount type=bind,src=$(pwd)/dblogs,dst=/var/log/postgresql

    # make the command 'postgresql' global
    mkdir -p $DSDIR/cmd/
    cp $APPDIR/cmd/postgresql.sh $DSDIR/cmd/
}
