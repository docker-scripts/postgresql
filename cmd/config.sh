cmd_config() {
    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    ds inject postgresql.sh
}
