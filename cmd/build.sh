rename_function cmd_build ds_build
cmd_build() {
    [[ -z $PG_VERSION ]] && fail "No PG_VERSION defined on 'settings.sh'"
    ds_build --build-arg PG_VERSION=$PG_VERSION "$@"
}
