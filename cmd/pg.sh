cmd_pg_help(){
    cat <<_EOF
    pg <cmd> <args>
        Run postgres commands, like: psql, pg_dump, createdb, etc.
_EOF
}

cmd_pg() {
    [[ -n $1 ]] || fail "Usage:\n$(cmd_pg_help)"

    if test -t 0 ; then
        docker exec -it \
               -u postgres -w /var/lib/postgresql \
               $CONTAINER env TERM=xterm "$@"
    else
        docker exec -i \
               -u postgres -w /var/lib/postgresql \
               $CONTAINER "$@"
    fi
}
