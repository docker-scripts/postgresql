#!/bin/bash -x

if [[ -n $(ls /var/lib/postgresql/) ]]; then
    chown postgres: -R /var/lib/postgresql/
    chown postgres: -R /etc/postgresql/
    chown postgres: -R /var/log/postgresql/
else
    # we are installing for the first time
    cp -a /var/lib/postgresql1/* /var/lib/postgresql/
    cp -a /etc/postgresql1/* /etc/postgresql/

    # listen to any address
    sed -i /etc/postgresql/*/main/postgresql.conf \
        -e "/listen_addresses/ c listen_addresses = '*'"

    # allow access from the local network, with a plain password
    echo 'host all all samenet password' | tee --append /etc/postgresql/*/main/pg_hba.conf
fi

# restart the service
systemctl restart postgresql
